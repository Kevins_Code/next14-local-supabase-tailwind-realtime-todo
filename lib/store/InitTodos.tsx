"use client";
import { User } from "@supabase/supabase-js";
import React, { useEffect, useRef } from "react";

import { TodoType, useTodo } from "./todos";
import { LIMIT_MESSAGE } from "../constant";

export default function InitTodos({ todos }: { todos: TodoType[] }) {
	const initState = useRef(false);
	const hasMore = todos.length >= LIMIT_MESSAGE;

	useEffect(() => {
		if (!initState.current) {
			useTodo.setState({ todos, hasMore });
		}
		initState.current = true;
		// eslint-disable-next-line
	}, []);

	return <></>;
}
