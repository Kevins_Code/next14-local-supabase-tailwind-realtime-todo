import { User } from "@supabase/supabase-js";
import { create } from "zustand";
import { LIMIT_MESSAGE } from "../constant";

export type TodoType = {
  created_at: string;
  id: string;
  is_edit: boolean;
  is_done: boolean;
  todo: string;
  img_path: string;
  user_id: string;
};

interface TodoState {
  hasMore: boolean;
  page: number;
  todos: TodoType[];
  actionTodo: TodoType | undefined;
  optimisticIds: string[];
  addTodo: (todo: TodoType) => void;
  setActionTodo: (todo: TodoType | undefined) => void;
  optimisticDeleteTodo: (todoId: string) => void;
  optimisticUpdateTodo: (todo: TodoType) => void;
  setOptimisticIds: (id: string) => void;
  setTodos: (todo: TodoType[]) => void;
}

export const useTodo = create<TodoState>()((set) => ({
  hasMore: true,
  page: 1,
  todos: [],
  optimisticIds: [],
  actionTodo: undefined,
  setTodos: (todos) =>
    set((state) => ({
      todos: [...todos, ...state.todos],
      page: state.page + 1,
      hasMore: todos.length >= LIMIT_MESSAGE,
    })),
  setOptimisticIds: (id: string) =>
    set((state) => ({ optimisticIds: [...state.optimisticIds, id] })),
  addTodo: (newTodos) =>
    set((state) => ({
      todos: [...state.todos, newTodos],
    })),
  setActionTodo: (todos) => set(() => ({ actionTodo: todos })),
  optimisticDeleteTodo: (todoId) =>
    set((state) => {
      return {
        todos: state.todos.filter((todo) => todo.id !== todoId),
      };
    }),
  optimisticUpdateTodo: (row) =>
    set((state) => {

      const objIndex = useTodo.getState().todos.findIndex((obj) => {
        return obj.id == row.original.id;
      });

	  if (state.todos[objIndex].is_done) {
		state.todos[objIndex].is_done = false
	  } else {
		state.todos[objIndex].is_done = true
	  }
	  
	  return state

    }),
}));
