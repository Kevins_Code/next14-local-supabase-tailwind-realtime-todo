"use client";
import React, { useEffect, useState } from "react";
import { createClient } from "@/utils/supabase/client";
import { toast } from "sonner";
import { v4 as uuidv4 } from "uuid";
import { useUser } from "@/lib/store/user";
import { TodoType, useTodo } from "@/lib/store/todos";
import { BUCKET_NAME } from "@/lib/constant";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";

import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { Label } from "./ui/label";

const formSchema = z.object({
  todo: z.string().min(2, {
    message: "Todo must be at least 2 characters.",
  }),
  file: z.any(),
});

export function ProfileForm() {
  const user = useUser((state) => state.user);
  const addTodo = useTodo((state) => state.addTodo);
  const setOptimisticIds = useTodo((state) => state.setOptimisticIds);
  const supabase = createClient();

  const [selectedImage, setSelectedImage] = useState("");
  const [selectedFile, setSelectedFile] = useState<File>();

  const supaUser = async () => {
    const {
      data: { user },
    } = await supabase.auth.getUser();
    return user;
  };

  useEffect(() => {
    supaUser().then((user) => {
      useUser.setState({ user });
    });
  }, [useUser]);

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      todo: "",
    },
  });

  let uploadedPath: any;

  async function onSubmit(values: z.infer<typeof formSchema>) {
    // Do something with the form values.
    // ✅ This will be type-safe and validated.

    if (values.todo.trim()) {
      const id = uuidv4();

      if (selectedFile) {
        const { data, error: imgError } = await supabase.storage
          .from(BUCKET_NAME)
          .upload(selectedImage, selectedFile);

        if (imgError) {
          console.error(imgError.message);
          toast.error("Upload Error " + imgError);
        }

        if (data) {
          uploadedPath = data?.path;
        }
      }

      const newTodo = {
        id,
        todo: values.todo,
        is_edit: false,
        is_done: false,
        created_at: new Date().toISOString(),
        img_path: selectedImage,
        profiles: {
          email: useUser.getState().user?.email,
        },
        user_id: useUser.getState().user?.id,
      };

      addTodo(newTodo);
      setOptimisticIds(newTodo.id);
      const { error } = await supabase.from("todos").insert({
        todo: values.todo,
        id,
        user_id: useUser.getState().user?.id,
        img_path: uploadedPath,
      });
      if (error) {
        console.error(error.message);
        toast.error(error.message);
      }
      form.reset();
      setSelectedImage("");

      console.log(
        "Todo State ",
        useTodo.getState(),
        "User State ",
        useUser.getState()
      );
    } else {
      toast.error("Message field can not be empty!!");
    }
  }

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)}>
        <FormField
          control={form.control}
          name="todo"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Todo Description</FormLabel>
              <FormControl>
                <Input placeholder="shadcn" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <div className="grid w-full max-w-sm items-center gap-1.5 mt-2">
          <Label className="mb-1" htmlFor="file">
            Picture (Optional)
          </Label>
          <Input
            id="file"
            type="file"
            {...form.register("file")}
            onChange={({ target }) => {
              if (target.files) {
                const file = target.files[0];
                if (file) {
                  setSelectedImage(URL.createObjectURL(file));
                  setSelectedFile(file);
                }
              }
            }}
          />
        </div>
        <p
          className="text-sm text-gray-500 dark:text-gray-300"
          id="file_input_help"
        >
          {" "}
          SVG, PNG, JPG or GIF (MAX. 800x400px).
        </p>

        <Button type="submit">Submit</Button>
      </form>
    </Form>
  );
}
