"use client";

import * as React from "react";
import {
  ColumnDef,
  ColumnFiltersState,
  SortingState,
  VisibilityState,
  flexRender,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  useReactTable,
} from "@tanstack/react-table";
import { ArrowUpDown, ChevronDown, MoreHorizontal } from "lucide-react";
import { createClient } from "@/utils/supabase/client";

import { Button } from "@/components/ui/button";
import { Checkbox } from "@/components/ui/checkbox";
import {
  DropdownMenu,
  DropdownMenuCheckboxItem,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { Input } from "@/components/ui/input";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import { useTodo } from "@/lib/store/todos";
import { LIMIT_MESSAGE } from "@/lib/constant";
import { Fragment, useEffect } from "react";
import Image from "next/image";
import { createAdminClient } from "@/utils/supabase/adminclient";
import { useRouter } from "next/navigation";

export type Todo = {
  created_at: string;
  id: string;
  img_path: string;
  is_done: boolean;
  is_edit: boolean;
  todo: string;
  user_id: string;
};

export const columns: ColumnDef<Todo>[] = [
  {
    id: "select",
    enableSorting: false,
    enableHiding: false,
  },
  {
    accessorKey: "user_id",
    header: "User",
    cell: ({ row }) => {
      return (
        <Fragment>
          <div
            className={`font-medium ${
              row.original.is_done ? `line-through` : null
            }`}
          >
            {row.original.profiles ? row.original.profiles.email : null}
          </div>
        </Fragment>
      );
    },
  },
  {
    accessorKey: "todo",
    header: "Todo",
    cell: ({ row }) => {
      const [imgUrl, setImgUrl] = React.useState("");

      useEffect(() => {
        signedUrl().then((response) => {
          setImgUrl(response);
        });
      }, []);

      const signedUrl = async () => {
        const supabase = createClient();

        const { data, error } = await supabase.storage
          .from("todo-img")
          .createSignedUrl(row.original.img_path, 3600);

        return data?.signedUrl;
      };

      return (
        <Fragment>
          <div
            className={`font-medium ${
              row.original.is_done ? `line-through` : null
            }`}
          >
            {row.getValue("todo")}
          </div>

          {imgUrl ? (
            <Image
              className={`font-medium ${
                row.original.is_done ? `brightness-50` : null
              }`}
              src={imgUrl}
              width={70}
              height={70}
              alt="Picture of the author"
            />
          ) : null}
        </Fragment>
      );
    },
  },
  {
    accessorKey: "created_at",
    header: ({ column }) => {
      return (
        <Button
          variant="ghost"
          onClick={() => column.toggleSorting(column.getIsSorted() === "desc")}
        >
          Date Added
          <ArrowUpDown className="ml-2 h-4 w-4" />
        </Button>
      );
    },
    cell: ({ row }) => {
      const date = row.getValue("created_at") as string;

      const convertDate = new Date(date);
      const convertedDate = convertDate.toDateString();

      return (
        <div
          className={`font-medium ${
            row.original.is_done ? `line-through` : null
          }`}
        >
          {convertedDate}
        </div>
      );
    },
  },
  {
    id: "actions",
    enableHiding: false,
    cell: ({ row }) => {
      const payment = row.original;
      const router = useRouter();
      const supabase = createClient();

      const deleteTodo = async () => {
        const { error } = await supabase
          .from("todos")
          .delete()
          .eq("id", row.original.id);

        const filteredItems = useTodo.getState().todos.filter((todo) => {
          return todo.id !== row.original.id;
        });

        useTodo.setState({ todos: filteredItems });
      };

      const markDone = async () => {
        const { data, error } = await supabase
          .from("todos")
          .update({ is_done: true })
          .eq("id", row.original.id)
          .select();

        useTodo.getState().optimisticUpdateTodo(row);
        router.refresh();
      };

      const markTodo = async () => {
        const supabase = createClient();

        const { data, error } = await supabase
          .from("todos")
          .update({ is_done: false })
          .eq("id", row.original.id)
          .select();

          useTodo.getState().optimisticUpdateTodo(row);
          router.refresh();
      };

      return (
        <DropdownMenu>
          <DropdownMenuTrigger asChild>
            <Button variant="ghost" className="h-8 w-8 p-0">
              <span className="sr-only">Open menu</span>
              <MoreHorizontal className="h-4 w-4" />
            </Button>
          </DropdownMenuTrigger>
          <DropdownMenuContent align="end">
            <DropdownMenuLabel>Actions</DropdownMenuLabel>

            {useTodo.getState().todos[row.index].is_done ? null : (
              <DropdownMenuItem onClick={markDone}>Mark Done</DropdownMenuItem>
            )}
            {useTodo.getState().todos[row.index].is_done ? (
              <DropdownMenuItem onClick={markTodo}>
                Unmark Done
              </DropdownMenuItem>
            ) : null}

            <DropdownMenuItem>Edit</DropdownMenuItem>
            <DropdownMenuItem onClick={deleteTodo}>Delete</DropdownMenuItem>
          </DropdownMenuContent>
        </DropdownMenu>
      );
    },
  },
];

const supabase = createClient();

const todoQuery = async () => {
  const { data } = await supabase
    .from("todos")
    .select(
      `
    *,
    profiles(
      email
    )
      `
    )
    .range(0, LIMIT_MESSAGE)
    .order("created_at", { ascending: true });

  // console.log(useTodo.setState({ todos: data }));
  useTodo.setState({ todos: data });
};

export function TodoTable() {
  const [sorting, setSorting] = React.useState<SortingState>([]);
  const [columnFilters, setColumnFilters] = React.useState<ColumnFiltersState>(
    []
  );
  const [columnVisibility, setColumnVisibility] =
    React.useState<VisibilityState>({});
  const [rowSelection, setRowSelection] = React.useState({});

  useEffect(() => {
    todoQuery();
  }, []);

  const { todos: data } = useTodo((state) => state);

  const table = useReactTable({
    data,
    columns,
    onSortingChange: setSorting,
    onColumnFiltersChange: setColumnFilters,
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    onColumnVisibilityChange: setColumnVisibility,
    onRowSelectionChange: setRowSelection,
    state: {
      sorting,
      columnFilters,
      columnVisibility,
      rowSelection,
    },
  });

  return (
    <div className="w-full">
      <div className="flex items-center py-4">
        <Input
          placeholder="Filter todo..."
          value={(table.getColumn("todo")?.getFilterValue() as string) ?? ""}
          onChange={(event) =>
            table.getColumn("todo")?.setFilterValue(event.target.value)
          }
          className="max-w-sm"
        />
      </div>
      <div className="rounded-md border ">
        <Table>
          <TableHeader>
            {table.getHeaderGroups().map((headerGroup) => (
              <TableRow key={headerGroup.id}>
                {headerGroup.headers.map((header) => {
                  return (
                    <TableHead key={header.id}>
                      {header.isPlaceholder
                        ? null
                        : flexRender(
                            header.column.columnDef.header,
                            header.getContext()
                          )}
                    </TableHead>
                  );
                })}
              </TableRow>
            ))}
          </TableHeader>
          <TableBody>
            {table.getRowModel().rows?.length ? (
              table.getRowModel().rows.map((row) => (
                <TableRow
                  key={row.id}
                  data-state={row.getIsSelected() && "selected"}
                >
                  {row.getVisibleCells().map((cell) => (
                    <TableCell key={cell.id}>
                      {flexRender(
                        cell.column.columnDef.cell,
                        cell.getContext()
                      )}
                    </TableCell>
                  ))}
                </TableRow>
              ))
            ) : (
              <TableRow>
                <TableCell
                  colSpan={columns.length}
                  className="h-24 text-center"
                >
                  No results.
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </div>
      <div className="flex items-center justify-end space-x-2 py-4">
        <div className="flex-1 text-sm text-muted-foreground">
          {table.getFilteredSelectedRowModel().rows.length} of{" "}
          {table.getFilteredRowModel().rows.length} row(s) selected.
        </div>
        <div className="space-x-2">
          <Button
            variant="outline"
            size="sm"
            onClick={() => table.previousPage()}
            disabled={!table.getCanPreviousPage()}
          >
            Previous
          </Button>
          <Button
            variant="outline"
            size="sm"
            onClick={() => table.nextPage()}
            disabled={!table.getCanNextPage()}
          >
            Next
          </Button>
        </div>
      </div>
    </div>
  );
}
